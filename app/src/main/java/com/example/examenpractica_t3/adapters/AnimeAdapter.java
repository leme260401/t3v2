package com.example.examenpractica_t3.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.examenpractica_t3.R;
import com.example.examenpractica_t3.personaje.Anime;
import com.google.gson.Gson;

import java.util.List;

public class AnimeAdapter extends RecyclerView.Adapter<AnimeAdapter.AnimeViewHolder>{
    private List<Anime> listaAnimes;
    private TextView nombre;
    private TextView descripcion;
    private ImageView imagen;
    public String urlImage;
    private Button btnVer;

    public AnimeAdapter(List<Anime> listaAnimes) {
        this.listaAnimes = listaAnimes;
    }

    @NonNull
    @Override
    public AnimeAdapter.AnimeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_anime, parent, false);
        AnimeViewHolder vh = new AnimeViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AnimeAdapter.AnimeViewHolder holder, int position) {

        nombre = holder.itemView.findViewById(R.id.tvNombre);
        descripcion = holder.itemView.findViewById(R.id.tvDescripcion);
        Anime anime = listaAnimes.get(position);
        nombre.setText(anime.getNombre());
        descripcion.setText(anime.getDescripcion());
        imagen = holder.itemView.findViewById(R.id.ivImagen);
        urlImage = anime.getUrl_imagen();
        Glide.with(holder.itemView.getContext()).load(urlImage).into(imagen);
        /*
        btnVer = holder.itemView.findViewById(R.id.btnVer);
        btnVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, DetalleActivity.class);
                intent.putExtra("Pokemon",new Gson().toJson(pokemon));
                activity.startActivity(intent);
            }
        });
        */
    }

    @Override
    public int getItemCount() {
        return listaAnimes.size();
    }

    public class AnimeViewHolder extends RecyclerView.ViewHolder {

        public AnimeViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
