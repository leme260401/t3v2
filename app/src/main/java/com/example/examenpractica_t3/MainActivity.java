package com.example.examenpractica_t3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.examenpractica_t3.adapters.AnimeAdapter;
import com.example.examenpractica_t3.personaje.Anime;
import com.example.examenpractica_t3.servicios.AnimeService;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private androidx.recyclerview.widget.RecyclerView recyclerView;
    private ImageView imageView;
    private TextView nombre;
    private TextView descripcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rvAnime);
        imageView = findViewById(R.id.ivImagen);
        nombre = findViewById(R.id.tvNombre);
        descripcion = findViewById(R.id.tvDescripcion);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://vjksw.free.beeceptor.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AnimeService service = retrofit.create(AnimeService.class);

        service.all().enqueue(new Callback<List<Anime>>() {
            @Override
            public void onResponse(Call<List<Anime>> call, Response<List<Anime>> response) {
                if (response.code()==200){
                    List<Anime> animes = response.body();
                    Log.i("MY_APP" , new Gson().toJson(animes));
                    recyclerView.setAdapter(new AnimeAdapter(animes));
                }else {
                    Log.i("MY_APP" , "Intentalo de nuevo, algo falló");
                }
            }

            @Override
            public void onFailure(Call<List<Anime>> call, Throwable t) {
                Log.i("MY_APP" , "No pudimos conectarnos con la aplicación");
            }
        });
    }
}